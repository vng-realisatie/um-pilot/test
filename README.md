# UM Testen

_Versionering_  

| versie | datum          | opmerking      |
|--------|----------------|----------------|
| 1.0    | december 2022  | Toelichting bijgewerkt/layout |

_Begrippenlijst_

Postman
  : Postman is een applicatie om webservices/API's te testen.

Newman
  : Newman is een command-line Collection Runner voor Postman. M.a.w., het maakt het mogelijk om API testen gemaakt in Postman vanaf de command-line uit te voeren.


## Webservices/API's testen (technische testen)

### Postman tests uitvoeren
Deze GIT repo bevat Postman collection files, bestaande uit één of meerdere APUI testen, die in het programma Postman geimporteerd en uitgevoerd kunnen worden.

De collection files maken vaak gebruik van environment variabelen die doormiddel van een environment file in Postman geladen kunnen worden. Het gaat op het moment van schrijven om:
- de host waar de identificatie server (Keycloak) draait 
- de client id geconfigureerd in de identificatie server 
- de security realm geconfigureerd in de identificatie server
- eindgebruikers credentials, wederom geconfigureerd in de identificatie server

### Postman tests uitvoeren in docker

#### Test: Als gebruiker inloggen op het medewerkersportaal

Run commando waarbij env variabelen worden meegegeven
```console
docker run --rm --name postmanTests -v ${PWD}/webservices/postmanTests:/etc/newman -t postman/newman:alpine run \
--env-var clientIdFrontend="clientid-frontend-example" \
--env-var baseUrlIdentificationserver="https://um-admin.host-example.nl" \
--env-var realmBB="realm-example" \
--env-var uname="eindgebruiker-example" \
--env-var upassword="eindgebruikerswachtwoord-example" \
"Als gebruiker inloggen op het medewerkersportaal.postman_collection.json"
```
Run commando waarbij variabelen uit environment json file worden gehaald.
```console
docker run --rm --name postmanTests -v ${PWD}/webservices/postmanTests:/etc/newman -t postman/newman:alpine run \
--environment="testomgeving.postman_environment.json" \
"Als gebruiker inloggen op het medewerkersportaal.postman_collection.json"
```

Voorbeeld testresultaat:
```
Als gebruiker inloggen op het medewerkersportaal

→ login / authenticate
  POST https://um-admin.host-example.nl/auth/realms/realm-example/protocol/openid-connect/token [200 OK, 3.12kB, 587ms]
  ✓  Status code is 200
  ┌
  │ 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJRaGx1eDU1RDYtclczcTJiU3FOVTRRYWpya0dUcnRxMHp3aWxBa1RnSE5VIn0.eyJleHAiOjE2NjU0OTQ2NTMsImlhdCI6M
  │ TY2NTQ5NDM1MywianRpIjoiZmU5OGQ3MWUtOWM3ZC00NDBiLWE0MzYtMGZmNjFiYWYzOWU2IiwiaXNzIjoiaHR0cHM6Ly91bS1hZG1pbi50ZXN0ZG9ycC5ubC9hdXRoL3JlYWxtcy9wb2Mtdm5nLXJl
  │ YWxtIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjllNDFkOTkxLWVkMzYtNGEwNC05ZGU2LTYyODY2MTkwOTU1YyIsInR5cCI6IkJlYXJlciIsImF6cCI6InBvYy12bmctZnJvbnRlbmQiLCJzZXNzaW9
  │ uX3N0YXRlIjoiNWQ5YzI2YjEtNWM3ZS00NjA4LWE0ZjQtMjE3MTc0ODg4MTZiIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyJodHRwczovL3VtLnRlc3Rkb3JwLm5sIiwiKiJdLCJyZWFsbV
  │ 9hY2Nlc3MiOnsicm9sZXMiOlsiem9la2VuLXdlcmt6b2VrZW5kZSIsInVwbG9hZC12YWNhdHVyZXMiLCJvZmZsaW5lX2FjY2VzcyIsInpvZWtlbi12YWNhdHVyZXMiLCJkZWZhdWx0LXJvbGVzLXNwc
  │ mluZy1yZWFsbSIsInJhcHBvcnRhZ2UiLCJ1bWFfYXV0aG9yaXphdGlvbiIsInVzZXIiLCJ1cGxvYWQtd2Vya3pvZWtlbmRlIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6
  │ WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwic2lkIjoiNWQ5YzI2YjEtNWM3ZS00NjA4LWE0ZjQ
  │ tMjE3MTc0ODg4MTZiIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJvaW4iOiIwMDAwMDAwMTgyMTAwMjE5MzAwMCIsInByZWZlcnJlZF91c2VybmFtZSI6InZuZyJ9.G36727uwL7hg42uJX2WNQz-Ae
  │ iS9Azbufmx4w7NMGxF3EfUYcfWNc7uoMYfOaHLP2kiEeQY6i056G1vkZrsJnxlWXQbvcwFHHm1ZMrSN7JfKOxnLYymf2iPCxJoy_Ev4MOGgLtJBS97kxfO9CF2CTSZeeGMaHh4ClhFUmApAazHz_Sz0
  │ bdO2OFxXZVpLX6itZolN5ilhFIZYSh-brXn5VXuHvm4VjVK6QszWRtO-kkvoNzEpckrIcCT__Gl-lVUTD6sML_en7hACwaTRntYQK7cUUfMwHbNeino1DPbsR3y06dX7OLOeXmqBbbn-dKeWf7Sfs2y
  │ rgUm5FABqq4e0nA'
  └

┌─────────────────────────┬────────────────────┬───────────────────┐
│                         │           executed │            failed │
├─────────────────────────┼────────────────────┼───────────────────┤
│              iterations │                  1 │                 0 │
├─────────────────────────┼────────────────────┼───────────────────┤
│                requests │                  1 │                 0 │
├─────────────────────────┼────────────────────┼───────────────────┤
│            test-scripts │                  1 │                 0 │
├─────────────────────────┼────────────────────┼───────────────────┤
│      prerequest-scripts │                  0 │                 0 │
├─────────────────────────┼────────────────────┼───────────────────┤
│              assertions │                  1 │                 0 │
├─────────────────────────┴────────────────────┴───────────────────┤
│ total run duration: 797ms                                        │
├──────────────────────────────────────────────────────────────────┤
│ total data received: 2.42kB (approx)                             │
├──────────────────────────────────────────────────────────────────┤
│ average response time: 587ms [min: 587ms, max: 587ms, s.d.: 0µs] │
└──────────────────────────────────────────────────────────────────┘
```
